package com.devcamp.task56b20;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task56B20Application {

	public static void main(String[] args) {
		SpringApplication.run(Task56B20Application.class, args);
	}

}
